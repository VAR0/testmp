//
//  ClimaRouter.swift
//  TestingNative
//
//  Created by Victor O Luciano Alvarado on 6/02/2020.
//  Copyright © 2020 Develop Aso. All rights reserved.
//

import Foundation
import UIKit

class ClimaRouter : PresenterToRouterClimaProtocol {
    
    static func createClimaMtyModule() -> ClimaMtyViewController {
        let view = ClimaRouter.mainstoryboard.instantiateViewController(withIdentifier: "ClimaMtyViewController") as! ClimaMtyViewController
        let presenter: ViewToPresenterClimaProtocol & InteractorToPresenterClimaProtocol = ClimaPresenter()
        let interactor: PresenterToInteractorClimaProtocol = ClimaInteractor()
        let router: PresenterToRouterClimaProtocol = ClimaRouter()
        
        view.presenter = presenter
        presenter.view  = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
        
    }
    
    static func createClimaCdmxModule() -> ClimaCdmxViewController {
        let view = ClimaRouter.mainstoryboard.instantiateViewController(withIdentifier: "ClimaCdmxViewController") as! ClimaCdmxViewController
        let presenter: ViewToPresenterClimaProtocol & InteractorToPresenterClimaProtocol = ClimaPresenter()
        let interactor: PresenterToInteractorClimaProtocol = ClimaInteractor()
        let router: PresenterToRouterClimaProtocol = ClimaRouter()
        
        view.presenter = presenter
        presenter.view  = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
        
    }
    
    static func createClimaGdlModule() -> ClimaGdlViewController {
        let view = ClimaRouter.mainstoryboard.instantiateViewController(withIdentifier: "ClimaGdlViewController") as! ClimaGdlViewController
        let presenter: ViewToPresenterClimaProtocol & InteractorToPresenterClimaProtocol = ClimaPresenter()
        let interactor: PresenterToInteractorClimaProtocol = ClimaInteractor()
        let router: PresenterToRouterClimaProtocol = ClimaRouter()
        
        view.presenter = presenter
        presenter.view  = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
        
    }
    
    static var mainstoryboard : UIStoryboard {
        return UIStoryboard(name:"Main",bundle: Bundle.main )
    }
    
    
    
    
}
