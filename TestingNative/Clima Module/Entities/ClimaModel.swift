//
//  ClimaModel.swift
//  TestingNative
//
//  Created by Victor Luciano on 09/02/2020.
//  Copyright © 2020 Develop Aso. All rights reserved.
//

import Foundation
class Clima: Codable, CustomStringConvertible {
    var description: String {
        return " "
    }
    
    let main: Main
    let id: Int
    let name: String
    
}
class Main: Codable, CustomStringConvertible {
    var description: String{
        return "temp : \(temp)"
    }
    let temp: Double
    let temp_min: Double
    let temp_max: Double
    
}
