//
//  ClimaMtyViewController.swift
//  TestingNative
//
//  Created by Victor O Luciano Alvarado on 6/02/2020.
//  Copyright © 2020 Develop Aso. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ClimaMtyViewController: UIViewController, UIGestureRecognizerDelegate {
    var presenter:ViewToPresenterClimaProtocol?
    var manager = CLLocationManager()
    var latitud : CLLocationDegrees!
    var longitud : CLLocationDegrees!
    
    @IBOutlet weak var mapa: MKMapView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        mapa.delegate = self
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        //manager.startUpdatingLocation()
        manager.requestLocation()
        // Do any additional setup after loading the view.
        
        
        let localizacion = CLLocationCoordinate2DMake(25.6866142, -100.3161126)
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: localizacion, span: span)
        mapa.setRegion(region, animated: true)
        mapa.showsUserLocation = true
        
        
        //setPinUsingMKPointAnnotation(location: localizacion)
        
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        gestureRecognizer.delegate = self
        mapa.addGestureRecognizer(gestureRecognizer)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let localizacion = CLLocationCoordinate2DMake(25.6866142, -100.3161126)
        setPinUsingMKPointAnnotation(location: localizacion)
    }
    
    @objc func handleTap(_ gestureReconizer: UILongPressGestureRecognizer) {
        
        let location = gestureReconizer.location(in: mapa)
        let coordinate = mapa.convert(location, toCoordinateFrom: mapa)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        //mapView.addAnnotation(annotation)
        clearMap()
        setPinUsingMKPointAnnotation(location: annotation.coordinate)
    }
    
    func clearMap(){
        self.mapa.annotations.forEach {
            if !($0 is MKUserLocation) {
                self.mapa.removeAnnotation($0)
            }
        }
    }
    
    func setPinUsingMKPointAnnotation(location: CLLocationCoordinate2D){
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Estas aquí"
        //annotation.subtitle = "Detalle de la ubicación"
        //let coordinateRegion = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 800, longitudinalMeters: 800)
        //mapa.setRegion(coordinateRegion, animated: true)
        //mapa.addAnnotation(annotation)
        
        let geoCoder = CLGeocoder()
        let location2 = CLLocation(latitude: location.latitude, longitude: location.longitude)
        geoCoder.reverseGeocodeLocation(location2, completionHandler: { (placemarks, error) -> Void in
            
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            if let locationName = placeMark.location {
                print(locationName)
            }
            
            if let street = placeMark.thoroughfare {
                print(street)
                annotation.subtitle = street
            }
            
            if let city = placeMark.subAdministrativeArea {
                print(city)
                annotation.title = city
            }
            
            if let zip = placeMark.isoCountryCode {
                print(zip)
            }
            
            if let country = placeMark.country {
                print(country)
            }
            
            let coordinateRegion = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 800, longitudinalMeters: 800)
            self.mapa.setRegion(coordinateRegion, animated: true)
            self.mapa.addAnnotation(annotation)
            self.presenter?.startFetchClima(Latitud: String(location.latitude), Longitud: String(location.longitude))
            
        })
        
    }
    
    
    func showAlertMessage(with message: String){
        let alert = UIAlertController(title: "Clima:", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension ClimaMtyViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
        annotationView.canShowCallout = true
        annotationView.rightCalloutAccessoryView = UIButton.init(type: .detailDisclosure)
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("Pin seleccionado...")
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation else {
            return
        }
        
        //let urlString = "http://maps.apple.com/?sll=\(annotation.coordinate.latitude),\(annotation.coordinate.longitude)"
        let localizacion = CLLocationCoordinate2DMake(annotation.coordinate.latitude, annotation.coordinate.longitude)
    }
    
}

extension ClimaMtyViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.latitud = location.coordinate.latitude
            self.longitud = location.coordinate.longitude
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: (error)")
    }
}
extension ClimaMtyViewController: PresenterToViewClimaProtocol{
    
    func showClima(moviesp : Clima) {
        let message = "Temperatura: \(moviesp.main.temp)º " + "Min: \(moviesp.main.temp_min)º " + "Max: \(moviesp.main.temp_max)º "
        self.showAlertMessage(with: message)
    }
    
    
}
