//
//  ClimaInteractor.swift
//  TestingNative
//
//  Created by Victor O Luciano Alvarado on 6/02/2020.
//  Copyright © 2020 Develop Aso. All rights reserved.
//

import Foundation

class ClimaInteractor: PresenterToInteractorClimaProtocol {
    
    var presenter: InteractorToPresenterClimaProtocol?
    var dataTask: URLSessionDataTask?
    var latitud :String = ""
    var longitud:String = ""
    
    func fetchClima(Latitud: String, Longitud: String) {
        self.latitud = Latitud
        self.longitud = Longitud
        getResponse(onSuccess: { (clima) in
            self.presenter?.cityClima(ClimaList: clima)
        }) { (error) in
            print(error)
        }
        
    }
    
    func getResponse( onSuccess:@escaping (Clima)-> Void, onError:@escaping (NSError)-> Void) {
        let urlString = "https://api.openweathermap.org/data/2.5/weather?lat=\(self.latitud)&lon=\(self.longitud)&APPID=4ff0f8da096772ac1e99d022f3b93b06"
        let url = favoritosURL(from : urlString)
        let session = URLSession.shared
        dataTask = session.dataTask(with: url, completionHandler: { data, response, error in
            if let error = error as NSError?, error.code == -999 {
                DispatchQueue.main.async {
                    onError(error)
                }
                return
            } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                if let data = data {
                    DispatchQueue.main.async {
                        onSuccess(self.parse(data: data)!)
                    }
                }
            } else {
                print("Failure! \(response!)")
                DispatchQueue.main.async {
                    let userInfo: [String : Any]  = [
                        NSLocalizedDescriptionKey : "ServiceError" ,
                        NSLocalizedFailureReasonErrorKey : "Failure! \(response!)"
                    ]
                    onError(NSError.init(domain: "", code: 405, userInfo: userInfo))
                }
            }
        })
        dataTask?.resume()
    }
    
    func favoritosURL(from urlString : String) -> URL {
        let url = URL(string: urlString)
        return url!
    }
    
    func parse(data:Data)->Clima?{
        do {
            let decoder = JSONDecoder()
            let temp = try decoder.decode(Clima.self, from: data)
            return temp
        } catch  {
            print("JSON Error: \(error)")
            return nil
        }
    }
    
}
