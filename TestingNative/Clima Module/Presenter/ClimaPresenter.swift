//
//  ClimaPresenter.swift
//  TestingNative
//
//  Created by Victor O Luciano Alvarado on 6/02/2020.
//  Copyright © 2020 Develop Aso. All rights reserved.
//

import Foundation


class ClimaPresenter: ViewToPresenterClimaProtocol{

    var view: PresenterToViewClimaProtocol?
    
    var interactor: PresenterToInteractorClimaProtocol?
    
    var router: PresenterToRouterClimaProtocol?
    
    func startFetchClima(Latitud: String, Longitud: String) {
        interactor?.fetchClima(Latitud: Latitud, Longitud: Longitud)
    }
}

extension ClimaPresenter: InteractorToPresenterClimaProtocol{
    
    func cityClima(ClimaList: Clima) {
        view?.showClima(moviesp: ClimaList)
    }
    
}
