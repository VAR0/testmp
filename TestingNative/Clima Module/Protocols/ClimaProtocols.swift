//
//  ClimaProtocols.swift
//  TestingNative
//
//  Created by Victor O Luciano Alvarado on 6/02/20.
//  Copyright © 2020 Develop Aso. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterClimaProtocol: class {
    
    var view: PresenterToViewClimaProtocol? {get set}
    var interactor: PresenterToInteractorClimaProtocol? {get set}
    var router: PresenterToRouterClimaProtocol? {get set}
    
    func startFetchClima(Latitud:String,Longitud:String)
}

protocol PresenterToViewClimaProtocol: class {
    func showClima(moviesp:Clima)
}

protocol PresenterToRouterClimaProtocol: class {
    static func createClimaMtyModule()-> ClimaMtyViewController
    static func createClimaCdmxModule()-> ClimaCdmxViewController
    static func createClimaGdlModule()-> ClimaGdlViewController
}

protocol PresenterToInteractorClimaProtocol: class {
    var presenter:InteractorToPresenterClimaProtocol? {get set}
    func fetchClima(Latitud:String,Longitud:String)
}

protocol InteractorToPresenterClimaProtocol: class {
    func cityClima(ClimaList: Clima)
}
